import React, { Component } from 'react';
import './App.css';
import { Link } from "react-router-dom";
import { Input } from 'reactstrap'
import { Round } from 'funk-for-lazy'

class About extends Component {
    constructor(props) {
        super(props)
        this.state={
            number: 0
        }
        this.handleChange=this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({ number: e.target.value })
    }

    render() {
        return (
            <div>
                <h1>io sono la About</h1>
                <h3>sto in /about ma in più posso navigare in Home</h3>
                <Link to="/">home</Link>
                <p>inserisci un valore e te lo arrotondo tramite la mia libreria</p>
                <Input
                    value={this.state.number}
                    onChange={(e) => this.handleChange(e)}
                />
                <h3>{Round(this.state.number)}</h3>
            </div>
        );
    }
}

export default About;
