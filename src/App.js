import React, { Component } from 'react';
import './App.css';
import Router from './routing/Router'
import Figlio from './figlio'
import ModalExample from './Modal'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1> main header </h1>
          <Router>
            <Figlio ciao="io sono una props"/>
            <ModalExample />
          </Router>
        </header>
      </div>
    );
  }
}

export default App;
