import React, { Component } from 'react';
import { STable } from './component/Table/Table'

const Cell=(props) => {
    return (
        <tr>
            <td>{props.element.id}</td>
            <td>{props.element.number}</td>
            <td>{props.element.one}</td>
            <td>{props.element.bo}</td>
            <td>{props.element.gia}</td>
        </tr>
    );
}



class Tabellona extends Component {
    state={
        rows: [
            { id: "uno", number: "1", one: "1", bo: "one", gia: "erste" },
            { id: "due", number: "2", one: "10", bo: "two", gia: "zweite" },
            { id: "tre", number: "3", one: "11", bo: "three", gia: "dritte" },
            { id: "quattro", number: "4", one: "100", bo: "four", gia: "vierte" },
            { id: "uno", number: "1", one: "1", bo: "one", gia: "erste" },
            { id: "due", number: "2", one: "10", bo: "two", gia: "zweite" },
            { id: "tre", number: "3", one: "11", bo: "three", gia: "dritte" },
            { id: "quattro", number: "4", one: "100", bo: "four", gia: "vierte" },
            { id: "uno", number: "1", one: "1", bo: "one", gia: "erste" },
            { id: "due", number: "2", one: "10", bo: "two", gia: "zweite" },
            { id: "tre", number: "3", one: "11", bo: "three", gia: "dritte" },
            { id: "quattro", number: "4", one: "100", bo: "four", gia: "vierte" },
            { id: "uno", number: "1", one: "1", bo: "one", gia: "erste" },
            { id: "due", number: "2", one: "10", bo: "two", gia: "zweite" },
            { id: "tre", number: "3", one: "11", bo: "three", gia: "dritte" },
            { id: "quattro", number: "4", one: "100", bo: "four", gia: "vierte" },
            { id: "uno", number: "1", one: "1", bo: "one", gia: "erste" },
            { id: "due", number: "2", one: "10", bo: "two", gia: "zweite" },
            { id: "tre", number: "3", one: "11", bo: "three", gia: "dritte" },
            { id: "quattro", number: "4", one: "100", bo: "four", gia: "vierte" },
            { id: "uno", number: "1", one: "1", bo: "one", gia: "erste" },
            { id: "due", number: "2", one: "10", bo: "two", gia: "zweite" },
            { id: "tre", number: "3", one: "11", bo: "three", gia: "dritte" },
            { id: "quattro", number: "4", one: "100", bo: "four", gia: "vierte" },
            { id: "uno", number: "1", one: "1", bo: "one", gia: "erste" },
            { id: "due", number: "2", one: "10", bo: "two", gia: "zweite" },
            { id: "tre", number: "3", one: "11", bo: "three", gia: "dritte" },
            { id: "quattro", number: "4", one: "100", bo: "four", gia: "vierte" },
        ]
    }


    applyFilter() {
        console.log("wiiii");
    }
    render() {
        return (
            <div>
                <STable
                    dataSource={this.state.rows}
                    row={Cell}
                />
            </div>
        );
    }
}

export default Tabellona;