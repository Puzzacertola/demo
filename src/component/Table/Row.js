import React from 'react'

export const Row = (props) => {
    return(
        <tr>
            {props.element.map((x, index) => {
                return(<td key={index} >{x}</td>);
            })}
        </tr>
    );  
}