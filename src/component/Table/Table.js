import React, { Component, Fragment } from 'react'
import { Table } from 'reactstrap'
import PropTypes from 'prop-types'
import {Row} from './Row'
import PaginationTool from './Pagination'

export class STable extends Component{
    constructor(props){
        super(props)
        this.emptyMessage = this.emptyMessage.bind(this)
    }
    emptyMessage(text){
        return(
            <tr>
                <td colSpan="100%">
                    {text}
                </td>
            </tr>
        );
    }
    pageChanged(index){
        alert(index);
    }
    render(){
        const Riga = this.props.row;
        return(
            <Fragment>
                <Table>
                    <thead>
                        {this.props.header}
                    </thead> 
                    <tbody>
                        {this.props.dataSource.length >0 ?
                            this.props.dataSource.map((x, index) =>{return <Riga key={index} element={x} />})
                        : this.emptyMessage(this.props.emptyTableMessage)
                        }
                    </tbody>
                </Table>
                <PaginationTool changePage={this.pageChanged}/>
            </Fragment>
        );
    }  
}

STable.propTypes = {
    dataSource: PropTypes.array,
    emptyTableMessage: PropTypes.string,
    header: PropTypes.oneOfType([PropTypes.func, PropTypes.element]),
    row: PropTypes.oneOfType([PropTypes.func, PropTypes.element]),
}
  
STable.defaultProps = {
    dataSource: [],
    //header: {},
    emptyTableMessage: 'Nessun risultato',
    row: Row,
}
