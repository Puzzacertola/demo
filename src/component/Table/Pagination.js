import React from 'react'
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap'
import PropTypes from 'prop-types'

class PaginationTool extends React.Component{

    gotoPage = (e, pageIndex) => {
        if (this.props.changePage) {
            this.props.changePage(pageIndex)
        }
        e.preventDefault()
      }

      evaluatePageNumber = () => {
        const { rowNumber, rowOnPage } = this.props;
        if(rowNumber % rowOnPage === 0){
            return rowNumber / rowOnPage;
        }
        else {
            return (rowNumber/rowOnPage) +1;
        }
      }
      evaluateRowInterval(page){
        return 
      }

    render(){
        const {actualPage} = this.props;

        return(
            <Pagination >
                 <PaginationItem>
                    <PaginationLink previous onClick={(e) => this.gotoPage(e, 0)} />
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink onClick={(e) => this.gotoPage(e, 0)}>
                        1
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink onClick={(e) => this.gotoPage(e, 1)}>
                        2
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink onClick={(e) => this.gotoPage(e, 2)}>
                        3
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink next onClick={(e) => this.gotoPage(e, 3)} />
                </PaginationItem>
            </Pagination>
        );
    }
}

PaginationTool.propTypes = {
    actualPage: PropTypes.string,
    changePage: PropTypes.changePage,
    rowNumber: PropTypes.number,
    rowOnPage: PropTypes.number,
    pageNumber: PropTypes.number
}

export default PaginationTool