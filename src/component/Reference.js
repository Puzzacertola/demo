import React from 'react'

class Reference extends React.Component {
    constructor(props) {
        super(props)

        this.refObj=React.createRef();
    }

    componentDidMount() {
        // se fosse un elemento da scaricare come un'immagine non vedremmo niente perchè l'operazione 
        // di download e di inserimento nel dom non è conclusa
        // se facciamo console.log(this.divRef) vediamo invece tutto perchè la console dei browser 
        // vanno in callback
        // dovrei usare this.divRef.current.addEventListener('load', this.callbackFunction)
        console.log("did mount ", this.divRef.current)
    }

    render() {
        return (
            <div ref={this.divRef}>sono un div collegato al dom</div>
        );
    }
}