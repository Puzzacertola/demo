import React, { Component } from 'react';
import { Link } from "react-router-dom";
import styles from './users.module.scss';

class Users extends Component {
    render() {
        return (
            <div>
                <h1>sono la Users page</h1>
                <span className={styles.ciao}>sono una jsx quindi pù bella delle altre e vavigo dove mi pare</span>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/about/">About</Link>
                    </li>
                    <li>
                        <Link to="/users/">Users</Link>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Users;
