import React, { Component } from 'react';
import './App.css';
import { Link } from "react-router-dom";

const linkConfig={
    about: { path: "/about", name: "About" },
    users: { path: "/users", name: "Users" },
    tabellona: { path: '/tabellona', name: 'tabellona' }
}

class Figlio extends Component {

    renderLink(item) {
        const { path, name }=linkConfig[item]
        return (
            <li>
                <Link to={path}>{name}</Link>
            </li>
        );
    }
    render() {
        const { ciao }=this.props;

        return (
            <ul>
                <li>
                    <Link to={{ pathname: '/', state: { title: 'sono stata inviata tramite Link' } }} >Home</Link>
                </li>
                {this.renderLink('about')}
                {this.renderLink('users')}
                {this.renderLink('tabellona')}
                <p>{ciao}</p>
            </ul>
        );
    }
}

export default Figlio;