import React from 'react'
import Home from '../Home'
import About from '../About'
import Users from '../component/users/Users'
import Tabellona from '../Tabellona'


const NoMatch=() => {
    return (
        <div>
            <h2>404 Page Not Found</h2>
        </div>
    );
}

export const routes=[
    {
        title: "home",
        path: "/",
        component: Home,
        exact: true
    },
    {
        title: "About",
        path: "/about",
        component: About
    },
    {
        title: "Users",
        path: "/users",
        component: Users
    },
    {
        title: "Tabellona",
        path: "/tabellona",
        component: Tabellona
    },
    {
        title: "not found",
        path: "*",
        component: NoMatch
    },
]

