import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import {routes} from './routingMap'


class Router extends Component {

    drawRoutes() {
        return(
            routes.map( (x, index) =>
                 <Route 
                    key={index}
                    path={x.path} 
                    component={x.component} 
                    exact={x.exact? x.exact:false}
                 />
            )
        )
    }

    render() {
        return (
            <BrowserRouter>
                <div>
                    {this.props.children}
                    <Switch>
                        {this.drawRoutes()}
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}

export default Router;
